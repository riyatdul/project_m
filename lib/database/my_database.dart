import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../models/models.dart';

class MyDatabase {
  static final _databaseName = "movie_test.db";
  static final _tableName = "user";
  static final _dbV = 1;

  MyDatabase._privateConstructor();

  static final MyDatabase instance = MyDatabase._privateConstructor();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  _initDatabase() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, _databaseName);

    return await openDatabase(path, version: _dbV,
        onCreate: (db, version) async {
      var batch = db.batch();
      _createTableUser(batch);

      await batch.commit();
    });
  }

  void _createTableUser(Batch batch) async {
    batch.execute('''
          CREATE TABLE $_tableName (
            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
            email TEXT,
            username TEXT,
            password TEXT
          )
          ''');
  }

  Future<User> insert(User user) async {
    Database db = await instance.database;
    user.userId = await db.insert(_tableName, user.toJson());
    return user;
  }

  Future<User?> getUser(String email) async {
    Database db = await instance.database;
    var dataUser =
        await db.query(_tableName, where: "email = ?", whereArgs: [email]);

    if (dataUser.isNotEmpty) {
      return User.fromJson(dataUser.first);
    }
    return null;
  }
}
