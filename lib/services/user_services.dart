part of 'services.dart';

class UserServices {
  final database = MyDatabase.instance;

  Future<User?> registrasiUser(User user) async {
    User userd = await database.insert(user);
    if (userd != null) {
      print(userd.email);
      return userd;
    } else {
      return null;
    }
  }

  Future<User?> checkLogin(User user) async {
    User? userd = await database.getUser(user.email!);
    if (userd != null) {
      return userd;
    } else {
      return null;
    }
  }

  Future<void> setUser(String dataUser) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("user_value", dataUser);
  }

  Future<String?> getidUser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString("idUser");
  }

  Future<bool?> getlogin() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool("is_logged_in");
  }

  Future<void> setLogin() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool("is_logged_in", true);
  }
}
