import 'package:majootestcase/database/my_database.dart';
import 'package:majootestcase/models/models.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/utils.dart';

part 'api_service.dart';
part 'user_services.dart';
