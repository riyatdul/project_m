part of 'services.dart';

class ApiServices {
  Future<MovieModel?> getMovieList() async {
    try {
      final dio = await dioConfig.dio();
      final url = dio!.options.baseUrl + "3/trending/all/day";
      var response = await dioConfig.dioInstance?.get(
        url,
      );
      if (response?.statusCode == 200) {
        MovieModel movie = MovieModel.fromJson(response?.data);
        return movie;
      } else {
        return MovieModel();
      }
    } on ErrorHelper catch (e) {
      print(e.toString());
      return null;
    }
  }
}
