part of 'auths.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
        child: Column(
          children: [
            Text(
              'Daftar',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'Silahkan daftar untuk login',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(
              height: 9,
            ),
            Form(
                key: formKey,
                child: Column(
                  children: [
                    CustomTextFormField(
                      context: context,
                      controller: _usernameController,
                      hint: 'jhonDoe',
                      label: 'username',
                    ),
                    CustomTextFormField(
                      context: context,
                      controller: _emailController,
                      isEmail: true,
                      hint: 'Example@123.com',
                      label: 'Email',
                      validator: (val) {
                        final pattern =
                            new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                        if (val != null)
                          return pattern.hasMatch(val)
                              ? null
                              : 'email is invalid';
                      },
                    ),
                    CustomTextFormField(
                      context: context,
                      label: 'Password',
                      hint: 'password',
                      controller: _passwordController,
                      isObscureText: _isObscurePassword,
                      suffixIcon: IconButton(
                        icon: Icon(
                          _isObscurePassword
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                        ),
                        onPressed: () {
                          setState(() {
                            _isObscurePassword = !_isObscurePassword;
                          });
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomButton(
                      text: 'Registrasi',
                      onPressed: () async {
                        if (formKey.currentState?.validate() == true &&
                            _usernameController.value != null &&
                            _emailController.value != null &&
                            _passwordController.value != null) {
                          AuthBlocCubit authBlocCubit = AuthBlocCubit();
                          User user = User(
                            userName: _usernameController.value,
                            email: _emailController.value,
                            password: _passwordController.value,
                          );

                          await context.read<AuthBlocCubit>().daftarUser(user);
                          AuthBlocState state =
                              context.read<AuthBlocCubit>().state;
                          if (state is AuthBlocLoggedInState) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        HomeBlocScreen()));
                          } else {
                            print("Gagal registrasi");
                          }
                          authBlocCubit.loginUser(user);
                        }
                      },
                      height: 100,
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
