import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../bloc/blocs.dart';
import '../../../common/widget/widgets.dart';
import '../../../models/models.dart';
import '../home/homes.dart';

part 'login_page.dart';
part 'register_page.dart';
