part of 'movies.dart';

class MovieDetail extends StatelessWidget {
  final Results? results;
  const MovieDetail({Key? key, this.results}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0d253f),
      appBar: AppBar(
        backgroundColor: Color(0xff0d253f),
        elevation: 0.25,
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Icon(Icons.share),
          ),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Center(
            child: Column(
              children: [
                Container(
                    height: MediaQuery.of(context).size.width / 1.20,
                    width: MediaQuery.of(context).size.width / 2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(36),
                      image: DecorationImage(
                          fit: BoxFit.contain,
                          image: NetworkImage(
                              "https://image.tmdb.org/t/p/w500/" +
                                  results!.posterPath!)),
                    )),
                Text(
                    results?.title == null
                        ? "${results?.originalName}"
                        : "${results?.title}",
                    style: TextStyle(
                        fontSize: 26,
                        color: Colors.white,
                        fontWeight: FontWeight.w900),
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.ltr),
                SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 4,
                    ),
                    Icon(
                      Icons.star,
                      size: 24,
                      color: Color.fromARGB(255, 255, 154, 59),
                    ),
                    Text("${results?.voteAverage}",
                        style: TextStyle(
                            fontSize: 20,
                            color: Color.fromARGB(255, 255, 154, 59),
                            fontWeight: FontWeight.w900),
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.ltr),
                    SizedBox(
                      width: 10,
                    ),
                    Text("${dateFormat.format(results!.releaseDate!)}",
                        style: TextStyle(
                            fontSize: 16,
                            color: Color.fromARGB(66, 162, 247, 194),
                            fontWeight: FontWeight.w900),
                        textDirection: TextDirection.ltr),
                  ],
                ),
                SizedBox(
                  height: 24,
                ),
                Text("${results?.overview}",
                    style: TextStyle(fontSize: 14, color: Colors.white),
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.ltr)
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: ElevatedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ),
          child: Text('Beli Tiket'),
          onPressed: () {},
        ),
      ),
    );
  }
}
