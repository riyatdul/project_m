part of 'homes.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0d253f),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 26,
              ),
              Text("Explore",
                  style: TextStyle(
                      fontSize: 28,
                      color: Colors.white,
                      fontWeight: FontWeight.w900)),
              Text("Temukan film Favoritmu disini",
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.white,
                      fontWeight: FontWeight.w900)),
              SizedBox(
                height: 16,
              ),
              GridView.custom(
                scrollDirection: Axis.vertical,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 1 / 2,
                    mainAxisSpacing: 16,
                    crossAxisSpacing: 26,
                    crossAxisCount: 2),
                childrenDelegate: SliverChildListDelegate(data!
                    .map((e) => InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: ((context) => MovieDetail(
                                        results: e,
                                      ))));
                        },
                        child: movieItemWidget(e)))
                    .toList()),
              )
            ],
          ),
        ),
      ),
      // body: ListView.builder(

      //   itemCount: data?.length ?? 0,
      //   itemBuilder: (context, index) {
      //     return movieItemWidget(data![index]);
      //   },
      // ),
    );
  }

  Widget movieItemWidget(Results data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 8,
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage("https://image.tmdb.org/t/p/w500/" +
                        data.posterPath!))),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Expanded(
            flex: 1,
            child: Text(
                data.title == null ? "${data.originalName}" : "${data.title}",
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w900),
                textAlign: TextAlign.center,
                textDirection: TextDirection.ltr)),
        Expanded(
            flex: 1,
            child: Row(
              children: [
                SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.star,
                  size: 24,
                  color: Color.fromARGB(255, 255, 154, 59),
                ),
                Text("${data.voteAverage}",
                    style: TextStyle(
                        fontSize: 16,
                        color: Color.fromARGB(255, 255, 154, 59),
                        fontWeight: FontWeight.w900),
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.ltr),
                Spacer(),
                Text("${dateFormat.format(data.releaseDate!)}",
                    style: TextStyle(
                        fontSize: 14,
                        color: Color.fromARGB(66, 162, 247, 194),
                        fontWeight: FontWeight.w900),
                    textDirection: TextDirection.ltr),
              ],
            )),
      ],
    );
  }
}
