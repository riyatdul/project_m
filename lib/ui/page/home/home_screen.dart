part of 'homes.dart';

class HomeBlocScreen extends StatefulWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  State<HomeBlocScreen> createState() => _HomeBlocScreenState();
}

class _HomeBlocScreenState extends State<HomeBlocScreen> {
  @override
  void initState() {
    super.initState();
    context.read<HomeBlocCubit>().fetchingData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(data: state.data);
      } else if (state is HomeBlocLoadingState) {
        return LoadingIndicator();
      } else if (state is HomeBlocInitialState) {
        return Scaffold();
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(
          message: state.error,
          retry: () {
            context.read<HomeBlocCubit>().fetchingData();
          },
        );
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}
