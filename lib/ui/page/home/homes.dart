import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/page/movie/movies.dart';
import 'package:majootestcase/utils/constant.dart';

import '../../../bloc/blocs.dart';
import '../../../common/widget/widgets.dart';
import '../../../models/models.dart';

part 'home_loaded_screen.dart';
part 'home_screen.dart';
