import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/locator.dart';

import '../../../bloc/blocs.dart';
import 'ui/page/auth/auths.dart';
import 'ui/page/home/homes.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => AuthBlocCubit()),
        BlocProvider(create: (_) => HomeBlocCubit()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: BlocProvider(
          create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
          child: MyHomePageScreen(),
        ),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
      if (state is AuthBlocLoginState) {
        return LoginPage();
      } else if (state is AuthBlocLoggedInState) {
        // return BlocProvider(
        //   create: (context) => HomeBlocCubit()..fetchingData(),
        //   child: HomeBlocScreen(),
        // );
        return HomeBlocScreen();
      }

      return Scaffold(
        body: Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""),
        ),
      );
    });
  }
}
