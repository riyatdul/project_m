import 'package:get_it/get_it.dart';
import 'package:majootestcase/database/my_database.dart';

import 'services/services.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => ApiServices());
  locator.registerLazySingleton(() => UserServices());
}
