import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/bloc/blocs.dart';
import 'package:majootestcase/database/my_database.dart';
import 'package:majootestcase/models/models.dart';
import 'package:majootestcase/services/services.dart';

import '../locator.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  UserServices _userServices = locator<UserServices>();

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    bool? isLoggedIn = await _userServices.getlogin();
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  Future<void> loginUser(User user) async {
    User? valueUser = await _userServices.checkLogin(user);
    if (valueUser != null) {
      await _userServices.setLogin();
      String data = user.toJson().toString();
      await _userServices.setUser(data);
      emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocLoginState());
    }
  }

  Future<void> daftarUser(User user) async {
    emit(AuthBlocLoadingState());

    User? valueUser = await _userServices.registrasiUser(user);
    if (valueUser != null) {
      await _userServices.setLogin();
      String data = valueUser.toJson().toString();
      await _userServices.setUser(data);
      emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocErrorState("Gagal registrasi"));
    }
  }
}
