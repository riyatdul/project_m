import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/locator.dart';
import 'package:majootestcase/models/models.dart';
import 'package:majootestcase/utils/constant.dart';

import '../services/services.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());
  ApiServices _apiServices = locator<ApiServices>();

  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  void fetchingData() async {
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen((event) async {
      event = await _connectivity.checkConnectivity();
      if (event == ConnectivityResult.none) {
        emit(HomeBlocErrorState("Tidak ada koneksi internet"));
      } else {
        emit(HomeBlocInitialState());
        MovieModel? movieResponse = await _apiServices.getMovieList();
        if (movieResponse == null) {
          emit(HomeBlocErrorState("Error Unknown"));
        } else {
          emit(HomeBlocLoadedState(movieResponse.results));
        }
      }
    });
  }

  @override
  Future<void> close() {
    _connectivitySubscription.cancel();
    return super.close();
  }
}
