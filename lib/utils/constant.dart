import 'package:intl/intl.dart';

class Preference {
  static const USER_INFO = "user-info";
}

DateFormat dateFormat = DateFormat('dd MMM yyyy');

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
