part of 'models.dart';

class MovieModel {
  MovieModel({
    this.page,
    this.results,
    this.totalPages,
    this.totalResults,
  });

  int? page;
  List<Results>? results;
  int? totalPages;
  int? totalResults;

  factory MovieModel.fromJson(Map<String, dynamic> json) => MovieModel(
        page: json["page"],
        results:
            List<Results>.from(json["results"].map((x) => Results.fromJson(x))),
        totalPages: json["total_pages"],
        totalResults: json["total_results"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        // "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "total_pages": totalPages,
        "total_results": totalResults,
      };
}

class Results {
  Results({
    this.genreIds,
    this.originalLanguage,
    this.originalTitle,
    this.posterPath,
    this.video,
    this.voteAverage,
    this.id,
    this.releaseDate,
    this.voteCount,
    this.title,
    this.adult,
    this.backdropPath,
    this.overview,
    this.popularity,
    this.mediaType,
    this.firstAirDate,
    this.name,
    this.originalName,
    this.originCountry,
  });

  List<int>? genreIds;
  String? originalLanguage;
  String? originalTitle;
  String? posterPath;
  bool? video;
  double? voteAverage;
  int? id;
  DateTime? releaseDate;
  int? voteCount;
  String? title;
  bool? adult;
  String? backdropPath;
  String? overview;
  double? popularity;
  String? mediaType;
  String? firstAirDate;
  String? name;
  String? originalName;
  List<String>? originCountry;

  factory Results.fromJson(Map<String, dynamic> json) => Results(
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        originalLanguage: json["original_language"],
        originalTitle:
            json["original_title"] == null ? null : json["original_title"],
        posterPath: json["poster_path"],
        video: json["video"] == null ? null : json["video"],
        voteAverage: json["vote_average"].toDouble(),
        id: json["id"],
        releaseDate: json["release_date"] == null
            ? DateTime.now()
            : DateTime.parse(json["release_date"]),
        voteCount: json["vote_count"],
        title: json["title"] == null ? null : json["title"],
        adult: json["adult"] == null ? null : json["adult"],
        backdropPath: json["backdrop_path"],
        overview: json["overview"],
        popularity: json["popularity"].toDouble(),
        mediaType: json["media_type"],
        firstAirDate: json["first_air_date"],
        name: json["name"] == null ? null : json["name"],
        originalName:
            json["original_name"] == null ? null : json["original_name"],
        // originCountry: List<String>.from(json["origin_country"].map((x) => x)),
      );
}

// class MovieModel {
//   int? page;
//   List<Results>? results;
//   int? totalPages;
//   int? totalResults;

//   MovieModel({this.page, this.results, this.totalPages, this.totalResults});

//   MovieModel.fromJson(Map<String, dynamic> json) {
//     page = json['page'];
//     if (json['results'] != null) {
//       results = <Results>[];
//       json['results'].forEach((v) {
//         results!.add(new Results.fromJson(v));
//       });
//     }
//     totalPages = json['total_pages'];
//     totalResults = json['total_results'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['page'] = this.page;
//     if (this.results != null) {
//       data['results'] = this.results!.map((v) => v.toJson()).toList();
//     }
//     data['total_pages'] = this.totalPages;
//     data['total_results'] = this.totalResults;
//     return data;
//   }
// }

// class Results {
//   double? voteAverage;
//   String? overview;
//   String? releaseDate;
//   bool? adult;
//   String? backdropPath;
//   int? voteCount;
//   List<int>? genreIds;
//   int? id;
//   String? originalLanguage;
//   String? originalTitle;
//   String? posterPath;
//   String? title;
//   bool? video;
//   double? popularity;
//   String? mediaType;
//   String? name;
//   String? originalName;
//   List<String>? originCountry;
//   String? firstAirDate;

//   Results(
//       {this.voteAverage,
//       this.overview,
//       this.releaseDate,
//       this.adult,
//       this.backdropPath,
//       this.voteCount,
//       this.genreIds,
//       this.id,
//       this.originalLanguage,
//       this.originalTitle,
//       this.posterPath,
//       this.title,
//       this.video,
//       this.popularity,
//       this.mediaType,
//       this.name,
//       this.originalName,
//       this.originCountry,
//       this.firstAirDate});

//   Results.fromJson(Map<String, dynamic> json) {
//     voteAverage = json['vote_average'];
//     overview = json['overview'];
//     releaseDate = json['release_date'];
//     adult = json['adult'];
//     backdropPath = json['backdrop_path'];
//     voteCount = json['vote_count'];
//     genreIds = List<int>.from(json["genre_ids"].map((x) => x));
//     id = json['id'];
//     originalLanguage = json['original_language'];
//     originalTitle = json['original_title'];
//     posterPath = json['poster_path'];
//     title = json['title'];
//     video = json['video'];
//     popularity = json['popularity'];
//     mediaType = json['media_type'];
//     name = json['name'];
//     originalName = json['original_name'];
//     originCountry = List<String>.from(json["origin_country"].map((x) => x));
//     firstAirDate = json['first_air_date'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['vote_average'] = this.voteAverage;
//     data['overview'] = this.overview;
//     data['release_date'] = this.releaseDate;
//     data['adult'] = this.adult;
//     data['backdrop_path'] = this.backdropPath;
//     data['vote_count'] = this.voteCount;
//     data['genre_ids'] = this.genreIds;
//     data['id'] = this.id;
//     data['original_language'] = this.originalLanguage;
//     data['original_title'] = this.originalTitle;
//     data['poster_path'] = this.posterPath;
//     data['title'] = this.title;
//     data['video'] = this.video;
//     data['popularity'] = this.popularity;
//     data['media_type'] = this.mediaType;
//     data['name'] = this.name;
//     data['original_name'] = this.originalName;
//     data['origin_country'] = this.originCountry;
//     data['first_air_date'] = this.firstAirDate;
//     return data;
//   }
// }
