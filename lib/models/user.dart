part of 'models.dart';

class User {
  int? userId;
  String? email;
  String? userName;
  String? password;

  User({this.userId, this.email, this.userName, this.password});

  User.fromJson(Map<String, dynamic> json)
      : userId = json['userId'],
        email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() =>
      {'email': email, 'password': password, 'username': userName};
}
